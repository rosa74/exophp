<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>
<body>

<?php

// Ecrire le code permettant d'afficher 5 messages différents avec la fonction Switch, 
// suivant les goûts musicaux de chacun : Rap, Jazz, Techno, Classique et autre
// Vous changerez manuellement le contenu de la variable $a pour afficher vos différents messages

    

$a = [ "Rap","Jazz","Techno","Classique","Autre"];
$a2 = rand (0, 4); 


?>
    
<!-- écrire le code avant ce commentaire -->
    <?php


switch ($a2) {
    case 0: 
        echo "J'aime la techno";
        break;
    case 1:
        echo "J'aime le rap";
        break;
    case 2:
        echo "J'aime le jazz";
        break;
    case 3: 
        echo "J'aime le classique";
        break;
    default : 
        echo "J'aime autre style";
        
        
        
}

?>

<!-- écrire le code avant ce commentaire -->

</body>
</html>

