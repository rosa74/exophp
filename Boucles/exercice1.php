<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>
<body>

<?php

// Ecrire une boucle permettant d'afficher les 11 valeurs de $x en commençant par 0

?>

<!-- écrire le code après ce commentaire -->
<?php
$x = 0;
    while ($x <=10) {
        echo '$x valeur de x'.$x. '<br>';
        $x++;
    }
?>
<!-- écrire le code avant ce commentaire -->

</body>
</html>

