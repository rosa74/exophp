<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>
<body>

<?php

// Ecrire une boucle permettant d'afficher $x de 1 à 101 en comptant de 10 en 10

?>

<!-- écrire le code après ce commentaire -->
<?php

$x = 1;
    while ($x <=101) {
        echo '$x valeur de x'.$x. '<br>';
        $x+=10;
    }
?>
    
<!-- écrire le code avant ce commentaire -->

</body>
</html>
