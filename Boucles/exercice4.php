<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>
<body>

<?php

// Ecrire une boucle do ... while permettant d'afficher la valeur de base de $x qui sera de 30. Si c'est possible, affiche 
// sa suite numérique jusqu'à 0 en comptant de 4 en 4

?>

<!-- écrire le code après ce commentaire -->
<?php

$x=30;

do { 
    echo '$x contient la valeur de base'.$x. "<br>";
$x-=4;
}
while ($x > 0);

?>

<!-- écrire le code avant ce commentaire -->

</body>
</html>
